#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#define MAX_BUFFER 1024

int main()
{
	//Creación tuberia y apertura
	mkfifo("/tmp/tuberia", 0666);
	int fd=open("/tmp/tuberia", O_RDONLY);
	
	char cadena[MAX_BUFFER];
		
	if(fd<0){
	perror("error en la tuberia al intentar abrirla");
	}


	while(1){

			//leer datos e imprimir por pantalla
			read(fd,cadena,sizeof(cadena));
			printf("%s ", cadena);

			//if(strcmp(cadena, "Fin programa")==0)break;
	}

	//destrucción tuberia
	close (fd);
	unlink("/tmp/tuberia");

	return 0;
	//fin codigo tuberia


}
