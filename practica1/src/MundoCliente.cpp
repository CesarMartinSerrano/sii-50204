//#include "Mundo.h"
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
		// cerrar tuberia cliente
		close(fdc);
		unlink ("tmp/tuberiacliente");
		// cirre tuberia cliente-servidor
		close(fdClientSer);
		unlink ("tmp/tuberiaclienteservidor");
		
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

	//memcompartida.OnKeyboardDown()
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	

	//CODIGO BOT
	//intentando corregir error
	Memory->raqueta1 = jugador1;
	Memory->esfera = esfera;
	

	if(Memory->accion==1)OnKeyboardDown('w',0,0);
	else if(Memory->accion==-1)OnKeyboardDown('s',0,0);
	//fin codigo bot

	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		finpartida=0;
		if(puntos2==3) finpartida=1;
		if(finpartida==1) 
		{
			sprintf(cadena, "FIN DE PARTIDA");
			esfera.velocidad.x=0;
			esfera.velocidad.y=0;
		}
		esfera.radio=0.5f;
		
		char cadena[300];
		//sprintf(cadena, "Jugador2 anota un tanto y lleva un total de %d puntos \n", puntos2);
		//write(fd, cadena, sizeof(cadena));
		//write(fdc, cadena, strlen(cadena)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		finpartida=0;
		if(puntos1==3) finpartida=1;
		if(finpartida==1) 
		{
			sprintf(cadena, "FIN DE PARTIDA");
			esfera.velocidad.x=0;
			esfera.velocidad.y=0;
		}
		esfera.radio=0.5f;
		
		//char cadena[300];
		//sprintf(cadena, "Jugador1 anota un tanto y lleva un total de %d puntos \n", puntos1);
		//write(fd, cadena, sizeof(cadena));
		//write(fdc, cadena, strlen(cadena)+1);	

// ya esta el código read arriba
		//read(fdc, cadena, strlen(cadena)+1);
	}

//Cliente
		char cadena[200];
		read(fdc, cadena, sizeof(cadena));
		sscanf(cadena,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	char cad[100];
	sprintf(cad,"%c",key);
	write(fdClientSer, cad, sizeof(cad));
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//codigo de memoria compartida
	//

	// El logger queda comentando ya que el cliente esta conectado al bot
	//////
		//LOGGER
		//fd = open("/tmp/tuberia", O_WRONLY);
		//if (fd<1) perror("Error al abrir la tuberia/n/n");
		//fin logger
	
		//BOT
		//abrir fichero, crear, leer y escribir
		mem=open("/tmp/dirbot",O_CREAT|O_RDWR,0777);	

		write(mem,&MemComp, sizeof(MemComp));
		Memory=(DatosMemCompartida*) mmap(NULL,sizeof	(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,mem,0);

		//cerrar
		close(mem);
		//fin codigo bot

//cliente:

		mkfifo("/tmp/tuberiacliente", 0777);
		fdc = open("/tmp/tuberiacliente", O_RDWR);
		
		//rdonly
		if(fdc<0) perror("Error al abrir la tuberia del CLIENTE");


//Cliente-Sevidor
		
		mkfifo("/tmp/tuberiaclienteservidor", 0777);
		fdClientSer = open("/tmp/tuberiaclienteservidor", O_RDWR);
		
		// wronly
		if(fdClientSer<0) perror("Error al abrir la tuberia de CLIENTE-SERVIDOR");


//Socket

		sdComunicacion = socket(AF_INET, SOCK_STREAM,0);




		

}
