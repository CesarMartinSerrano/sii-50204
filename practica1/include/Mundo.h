// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "sys/mman.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#include "Esfera.h"
//#include "Raqueta.h"

#include "DatosMemCompartida.h"

#define MAX_BUFFER 1024

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

//BOT
	DatosMemCompartida MemComp;
	DatosMemCompartida *Memory;
	int mem;

	
	char cadena[MAX_BUFFER];
	
//LOGGER
	//tuberia y contaje de puntos
	//variables declaracdas
	int fd;
	int puntos1;
	int puntos2;
//propuesto
	int modoautomatico;
	int finpartida;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
